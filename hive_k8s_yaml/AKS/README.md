# HIVE


## deployment


` kubectl create configmap hive-site --from-file=hive-site.xml --from-file=hive-env.sh `  

Remember to put the configuration to comfigmap first.  

Run both of these lines  
` kubectl apply -f hive.yaml -n service`  
` kubectl apply -f hive-service.yaml -n service `   

## About configmap hot fixing

If you're gonna hot fix configurarion. run this line:  
`kubectl create configmap hive-site --from-file=hive-site.xml --from-file=hive-env.sh -o yaml --dry-run=client  | kubectl apply -f `  
and delete the existing `hive` pod, force pod for re-loading configmap.  